import { Router } from 'express';
import * as controller from './controller';

export const router = Router()
  .get('/marvel', controller.marvel)


