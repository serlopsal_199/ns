import * as express from "express";
import { join } from 'path';
import { readFile } from 'fs';
import { router } from './router';

const app = express();

app.use('/api', router);

app.use(
  express.Router().get('/', (req: express.Request, res: express.Response) => {
    const template = join(__dirname, 'index.html');
    readFile(template, 'utf8', (err, data) => {
      const body = err ? 'Nicasource.llc gives you a fraternal welcome to this talk.' : data;
      res.status(200).send(body);
    });
  })
);

app.listen(3000, () => {
  console.log(`running on port 3000`);
})
